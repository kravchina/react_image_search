# Image Search

## How to start the application

Open tabs in console in the root of project.
At first you should install all dependencies:

``npm install``

At second step you can run the application:

``npm start``

Application will be run and opened in browser.
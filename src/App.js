import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      value: ''
    };
    this.onSearch = this.onSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  onSearch(event) {
    event.preventDefault();
    axios({
        method: 'get',
        url: `https://api.imgur.com/3/gallery/search/?q=${this.state.value}`,
        headers: { 'Authorization': 'Client-ID f9ec2ceeb74341d' }
      })
      .then((res) => {
        this.setState({
          images: res.data.data
        });
      });
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  render() {
    return (
      <div className="app">
        <header className="header">
          <h1 className="title">Image Search</h1>
        </header>
        <div className="toolbox">
          <form onSubmit={this.onSearch}>
            <input type="text" value={this.state.value} onChange={this.handleChange} />
            <button type="submit">Search</button>
          </form>
        </div>
        <div className="result">
          {
            this.state.images.length <= 0 ?
              <div>No results on this image</div>
              : this.state.images.map((image) => {
                const imgUrl = image.cover ? 'https://i.imgur.com/' + image.cover + '.jpg' : image.link;
                return (<img key={image.id} src={imgUrl}/>)
              })
          }
        </div>
      </div>
    );
  }
}

export default App;
